FROM node:8-alpine
WORKDIR /competitionService/functions
COPY . /competitionService
RUN npm install -g firebase-tools
RUN npm install
ENV GOOGLE_APPLICATION_CREDENTIALS /userService/dev-competition-service-account-file.json
EXPOSE 8082

CMD [ "node", "index.js" ]
