const express = require("express");
const cors = require("cors");
const app = express();

const allowedOrigins = ["http://localhost:4200", "http://localhost:8080"];
const PORT = process.env.PORT || 8082;
const HOST = "0.0.0.0";
app.use(
  cors({
    origin: function(origin, callback) {
      // allow requests with no origin
      // (like mobile apps or curl requests)
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        var msg =
          "The CORS policy for this site does not " +
          "allow access from the specified Origin.";
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    }
  })
);
app
  .use(express.urlencoded({ extended: true }))
  .use(express.json())
  .use("/competitions", require("./lib/competitions/route"))
  .use("/ideas", require("./lib/ideas/route"))
  .use("/comments", require("./lib/comments/route"))
  .get("*", (_, res) =>
    res.status(404).json({ success: false, data: "Endpoint not found" })
  );
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

module.exports = app;
