const admin = require("firebase-admin");

const db = admin.firestore();

async function createComment(_req, res) {
  let comment = _req.body;
  const competitionId = comment.competitionId;
  const ideaId = comment.ideaId;
  const serverTimeStamp = admin.firestore.Timestamp.now();

  try {
    const competitionRef = db.collection("competitions").doc(competitionId);
    const ideaRef = competitionRef.collection("ideas").doc(ideaId);
    const commentRef = ideaRef.collection("comments").doc();
    comment.id = commentRef.id;
    comment.createdAt = serverTimeStamp;
    await commentRef.add(competition);
    return res.json({
      success: true,
      data: comment
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function deleteComment(_req, res) {
  const comment = _req.body;
  console.log("TCL: deleteComment -> comment", comment);
  //comment creator
  const authorId = comment.authorId;
  //authenticated user
  const authId = _req.authId;
  const competitionId = comment.competitionId;
  const ideaId = comment.ideaId;
  const commentId = comment.id;
  console.log("TCL: deleteComment -> commentId", commentId);
  try {
    const competitionRef = db.collection("competitions").doc(competitionId);
    const ideaRef = competitionRef.collection("ideas").doc(ideaId);
    const commentRef = ideaRef.collection("comments").doc(commentId);
    if (authId === authorId) {
      await commentRef.delete();
    } else {
      throw new Error("user do not have permission");
    }
    return res.json({
      success: true,
      data: "comment was successfully deleted from idea"
    });
  } catch (error) {
    console.log("TCL: deleteComment -> error", error);
    return res.send({
      success: false,
      data: error.toString()
    });
  }
}
async function getPaginatedComments(_req, res) {
  let competitionId = await _req.params.competitionId;
  const index = (await parseInt(_req.query.limit)) || 1;
  let ideaId = await _req.params.ideaId;

  let comments = [];
  try {
    const competitionRef = db.collection("competitions").doc(competitionId);
    const ideaRef = competitionRef.collection("ideas").doc(ideaId);
    const commentRef = ideaRef
      .collection("comments")
      .orderBy("createdAt")
      .limit(index);
    const idea = (await ideaRef.get()).data();
    let first = commentRef;
    let paginate = await first.get();

    // ...

    // Get the last document
    let last = paginate.docs[paginate.docs.length - 1];

    // Construct a new query starting at this document.
    // Note: this will not have the desired effect if multiple
    // cities have the exact same population value.
    let next = await ideaRef
      .collection("comments")
      .orderBy("createdAt")
      .startAt(last.data().createdAt)
      .limit(10)
      .get();

    next.forEach(doc => {
      comments.push(doc.data());
    });

    return res.json({
      success: true,
      data: {
        comments
        //idea
      }
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

module.exports = { getPaginatedComments, createComment, deleteComment };
